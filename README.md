[🇷🇺 Русская версия](./README_ru.md)

# My college grade project 

This is my college grade project (2021). It's subject was to make an RC robot
(a vehicle in my case) based on Arduino.

#### Warning: no code changes since [#0abf193b](https://gitlab.com/Night_H4nter/grade-firmware/-/commit/0abf193bb7af7ba04a5d9ca61934f59291a1dc6d) were tested on the vehicle itself. If you want to replicate the project or use it as a base, and the latest code doesn't work, then you can fall back to that commit or use the [last-tested](https://gitlab.com/Night_H4nter/grade-firmware/-/tree/last-tested) branch. It is the latest version that was tested, it was the version that made into the final grade project, thus it's guaranteed to work.

## The idea

If you're familiar with \*nix systems at least a bit, then you should probably know what vim is, so skip to the next paragraph. Otherwise proceed with reading.
Vim is a so called "modal" text editor: it has several modes in which the same
keys do different things. E.g. there's a "normal" mode that allows keys `h`,
`j`, `k` and `l` (by default) to be used to move the cursor left, down, up and
right, respectively. There's also an "insert" mode, where these keys, along with
all others, are used to just input the corresponding characters; this and other
modes are out of the scope for this project. Aside from that, in all modes 
except "insert", vim allows for specifying the amount of times a certain action
should be performed. For example, pressing `5` followed by `l` will result in
moving the cursor by five characters to the right; pressing `3` followed by `dd`
will result in three lines being deleted, going down from the one the cursor is
currently on.

The idea is to make a robot with vim-like controls, meaning it should behave
similar to vim's "normal" mode: an operator should be able to specify how far
should the vehicle travel forward or backwards, or what angle (in degrees)
should it rotate clockwise/anti-clockwise for. The latter wasn't implemented in code, since the hardware choice (not done by me) was a bit off, so the vehicle
could barely rotate.

## The implementation

#### The Hardware

The picture below shows how the hardware was interconnected. Check
[assets/principal.png](./assets/principal.png) for the same scheme in higher
size and pixel density (warning: huge file).

![principal_sheme](./assets/principal.jpg)

##### Note: wires that connect HC-05 `TXD` and `RXD` to Arduino's `RX` and `TX` should be unplugged during firmware flashing, since those pins share the serial interface controller with the one that the programmer is using to flash the firmware.

##### Bill of materials (electrical parts only)

| Symbol   | Name                                  | Quantity |
|----------|---------------------------------------|----------|
| DD1      | L298N motor driver                    |    1     |
| DD2      | Arduino Uno                           |    1     |
| DD3      | HC-05 Bluetooth receiver              |    1     |
| SW1      | Button switch                         |    1     |
| B1       | Battery block (6 type AA batteries)   |    1     |
| R1       | 1 kOhm resistor                       |    1     |
| R2       | 2 kOhm resistor                       |    1     |
| M1\.\.M4 | TA0132 Servo motor with a wheel       |    4     |
| N/A      | Wires (**!** with a breadboard used **!**)    |    19    |

****Warning:***  TA0132 motors are presumably too weak to carry this vehicle's
weight. Consider choosing something more power efficient or straight up more
powerful (you'll probably need more voltage in the latter case). Also keep
center of gravity in mind to split the load equally between motors, which
might've been the cause of rotation issue in my case.*

#### The firmware

The firmware is mostly ~~self~~ documented by comments. But several things
should be described or explained here.

ASCII characters must be sent to control the vehicle. The controls
(except `s` and number limits) correspond to vim's ones (see table below).


| Symbol | Description |
|--------|-------------|
| `h`    | rotate anti-clockwise   |
| `j`    | move back   |
| `k`    | move forward |
| `l`    | rotate clockwise |
| `s`    | stop immediately |
| `0..9` | set/change distance to travel forward/backwards |


Distance is measured in decimeters (10 cm). It can only be in the range from 0
to 99, since the bluetooth module works on Bluetooth 2.0 protocol which is
spec'ed to receive a signal from within 10 meters range.
Two digit values are achieved by shifting the existing value by one order of
magnitude, e.g. if the current value is `6`, and `5` is received, the value
becomes `65`. If the resulting value exceeds 99, then the value gets replaced
by the newer one, e.g. if the current value is `34`, and `7` is received,
then the new value becomes `7`.

Distance is specified by sending one or more digits. In order to move by
specified distance, one of the corresponding commands should be sent (`j` or
`k`) after specifying the distance. If the distance is `0` or unspecified, it's
assumed to to be `1`. Distance value is dropped if rotation or stop command
(`h`, `l` or `s`) is received.

A smartphone with an app capable of sending custom commands over bluetooth is
used as a remote. In my case, the app used was
[Bluetooth Controller HC-05](https://play.google.com/store/apps/details?id=com.mightyit.gops.bluetoothcontroller) (Google play).
This is the layout I used. It contains all symbols recognised by the vehicle:

<img src="./assets/mobileapplayout.jpg" width="33%" height="33%">

All the button names correspond to what they send. Sliders and `Btn 16..20` buttons are unused. Carriage return symbols and line feeds are turned off,
e.g. settings for `h` button look like this:

<img src="./assets/settingsexample.jpg" width="33%" height="33%">
