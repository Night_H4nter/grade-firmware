//what is referred to as action, action type, etc in comments,
//is what the vehicle actually does, not the objects of class 'action'
#include "action.h"



//depending on the character received, return an action type
actiontype action::actiontypefromchar(char input) {
    switch (input) {
        case 'h':
            return left;
        case 'j':
            return backwards;
        case 'k':
            return forward;
        case 'l':
            return right;
        case 's':
            return stop;
        default:
            return ignore;
    }
}



//basically convert char to int and return it
//IT DOESN'T WORK THE "SIMPLE WAY" DON'T YELL AT ME FOR THIS thanks
unsigned int action::countfromchar(char input) {
    String temp = String( input );
    return temp.toInt();
}



//if input char is a letter, it's an action type, so set it
//if input char is a digit, then handle count setting/increasing
//otherwise ignore input
void action::analyzeinput(char input_c) {
    if ( isAlpha( input_c ) )
        this->move = actiontypefromchar( input_c );
    else if ( isDigit( input_c ) ) {
        if ( this->count == 0 || this->count >= 10 )
            this->count = countfromchar( input_c );
        else if ( this->count < 10 )
            this->count = this->count * 10 + countfromchar( input_c );
    }
}



//handle calculating the delay from count number
//count is ignored on rotation anyways, otherwise should be set to 1
//in case it's omitted in serial command
unsigned long action::delayfromcount(int count) {
    if ( count == 0)
        count = 1;
    return count * this->dmtime;
}



//analyze the class fields, make a move, update the vehicle state
//and report if successful or not
bool action::makemove(int count, actiontype move) {
    switch (move) {
        case left:
            smartmovement::smartrotateanticlockwise();
            return true;
        case right:
            smartmovement::smartrotateclockwise();
            return true;
        case forward:
            smartmovement::smartmoveforward( delayfromcount ( count ) );
            return true;
        case backwards:
            smartmovement::smartmovebackwards( delayfromcount ( count ) );
            return true;
        case stop:
            smartmovement::smartstop();
            return true;
        default:
            smartmovement::performdelayedstop();
            return false;
    }
}



//perform the action
void action::perform() {
    bool movementperformed = makemove( this->count, this->move );
    //if action is successful, "clear" the values
    if ( movementperformed == true ) {
        this->count = 0;
        this->move = ignore;
        movementperformed = false;
    }
}
