//this is where the input gets processed and and the translated into actions
#ifndef ACTION_H
#define ACTION_H


#include <string.h>
#include "smartmovement.h"


//define an enum for every action (rotate left/right, move back/forward, stop)
//'ignore' is needed to make object not be cleared upon next iteration when
//there's no input during loop iteration (humans are slower than even arduino)
//basically means exactly "shut up and do nothing this iteration"
enum actiontype { left, backwards, forward, right, stop, ignore };



//define a general class for for all actions
class action {
    //no need for a complicated constructor function,
    //so just set the defaults here
    private:
        //decimeter (10 centimeters) is selected as a measurement unit
        //so this is roughly measured time in milliseconds
        //that it takes the vehicle to move by one decimeter
        //forward/backwards;
        const unsigned long dmtime = 440;

        //type of action (don't confuse with action object) to be performed
        //next; 'ignore' upon initialization
        actiontype move = ignore;

        //amount times an action should be performed (only used for
        //forward/backwards movement since the vehicle is garbage
        //and can barely turn
        unsigned int count = 0;



        //compose a move type from char
        actiontype actiontypefromchar(char input);

        //compose/change the count from char
        unsigned int countfromchar(char input);

        //make a move
        bool makemove(int count, actiontype move);

        //calculate delay from count received
        unsigned long delayfromcount(int count);



    public:
        //analyze serial input
        void analyzeinput(char input_c);

        //perform movement
        void perform();
};



#endif
