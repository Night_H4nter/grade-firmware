#include "advmovement.h"



//move forward
void advmovement::moveforward() {
    basemovement::rfwd();
    basemovement::lfwd();
}


//move backwards
void advmovement::movebackwards() {
    basemovement::rbwd();
    basemovement::lbwd();
}


//stop
void advmovement::stopmovement() {
    basemovement::rstop();
    basemovement::lstop();
}


//rotate clockwise
void advmovement::rotateclockwise() {
    basemovement::rbwd();
    basemovement::lfwd();
}


//rotate anti-clockwise
void advmovement::rotateanticlockwise() {
   basemovement::lbwd();
   basemovement::rfwd();
}
