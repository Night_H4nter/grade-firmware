//these are more advanced movement abstractions
//they represent basic movement of the whole vehicle
#ifndef ADVMOVEMENT_H 
#define ADVMOVEMENT_H 


#include "Arduino.h"
#include "basemovement.h"



class advmovement : private basemovement {
    protected:
        //move forward
        static void moveforward();

        //move backwards
        static void movebackwards();

        //stop
        static void stopmovement();

        //rotate clockwise
        static void rotateclockwise();

        //rotate anti-clockwise
        static void rotateanticlockwise();
};


#endif
