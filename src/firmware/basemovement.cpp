#include "basemovement.h"



//right side forward
void basemovement::rfwd() {
    digitalWrite(IN2, LOW);
    digitalWrite(IN1, HIGH);
}


//right side backwards
void basemovement::rbwd() {
    digitalWrite(IN1, LOW);
    digitalWrite(IN2, HIGH);
}


//right side stop
void basemovement::rstop() {
    digitalWrite(IN1, LOW);
    digitalWrite(IN2, LOW);
}


//left side forward
void basemovement::lfwd() {
    digitalWrite(IN4, LOW);
    digitalWrite(IN3, HIGH);
}


//left side backwards
void basemovement::lbwd() {
    digitalWrite(IN3, LOW);
    digitalWrite(IN4, HIGH);
}


//left side stop
void basemovement::lstop() {
    digitalWrite(IN3, LOW);
    digitalWrite(IN4, LOW);
}
