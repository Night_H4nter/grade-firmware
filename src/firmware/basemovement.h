//a bunch of the most basic abstractions
//to make life easier down the road
//allow for individual control over vehicle sides' movement
#ifndef MOVEMENT_H
#define MOVEMENT_H


#include "Arduino.h"



//give digital pins 8-11 that are connected to in1-in4 of l298n
//monikers for convenience
static const int IN1 = 8;
static const int IN2 = 9;
static const int IN3 = 10;
static const int IN4 = 11;



class basemovement {
    protected: 
        //right side forward
        static void rfwd();

        //right side backwards
        static void rbwd();

        //right side stop
        static void rstop();

        //left side forward
        static void lfwd();

        //left side backwards
        static void lbwd();

        //left side stop
        static void lstop();
};


#endif
