#include "action.h"



//prepare to initialize serial
char receivedchar = NULL;
boolean newdata = false;
action actiontomake;



//initialize arduino
void setup() {
    //setup pins 8-11 for output to control motors with l298n
    pinMode(IN1, OUTPUT);
    pinMode(IN2, OUTPUT);
    pinMode(IN3, OUTPUT);
    pinMode(IN4, OUTPUT);
    //turn off all motors initially
    digitalWrite(IN1, LOW);
	digitalWrite(IN2, LOW);
	digitalWrite(IN3, LOW);
	digitalWrite(IN4, LOW);
    
    //setup serial
    Serial.begin(9600);
    Serial.println("Serial initialized.");
}



//start the main program loop
void loop() {
    getonechar();
    if (newdata == true) {
        Serial.println(receivedchar);
        newdata = false;
    }
    actiontomake.analyzeinput( receivedchar );
    actiontomake.perform();
    receivedchar = NULL;
}



//receive a character
void getonechar() {
    if (Serial.available() > 0) {
        receivedchar = Serial.read();
        newdata = true;
    }
}
