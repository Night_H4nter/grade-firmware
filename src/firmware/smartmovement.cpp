//smater wrapper functions around motion functions
/* #include "action.h" */
#include "smartmovement.h"


//schedule a non-blocking stop
void smartmovement::schedulestop( unsigned long stopdelay ) {
    stoptime = millis() + stopdelay;
}



//stop the vehicle and update it's state
void smartmovement::smartstop() {
    stopmovement();
    vehstate = still;
    //this MUST be here due to some quirks related to motors
    //without it, arduino just resets itself upon rotation reversing
    //(e.g. if it' rotating anti-clockwise, send a command to rotate clockwise)
    //so basically it's fixing hardware problems with firmware
    delay(200);
}



//if a scheduled vehicle stop exists, perform it if it's delay ran out
void smartmovement::performdelayedstop() {
    //if stop time isn't zero and it has ran out,
    //stop the vehicle and update it's status
    if ( stoptime != 0 && millis() >= stoptime ) {
        smartmovement::smartstop();
        stoptime = 0;
    }
}



//remove a scheduled stop, start rotating anti-clockwise,
//while considering the state, and update the vehicle state
void smartmovement::smartrotateanticlockwise() {
    stoptime = 0;
    if ( vehstate == rotcw ) {
        smartmovement::smartstop();
        rotateanticlockwise();
    } else if ( vehstate == mforward || vehstate == mback
            || vehstate == still )
        rotateanticlockwise();
    vehstate = rotacw;
}



//remove a scheduled stop, start rotating clockwise,
//while considering the state, and update the vehicle state
void smartmovement::smartrotateclockwise() {
    stoptime = 0;
    if ( vehstate == rotacw ) {
        smartmovement::smartstop();
        rotateclockwise();
    } else if ( vehstate == mforward || vehstate == mback 
            || vehstate == still )
        rotateclockwise();
    vehstate = rotcw;
}



//perform delayed stop, if it exists, start moving forward,
//schedule a stop, and update the vehicle state
void smartmovement::smartmoveforward( unsigned long int delay ) {
    smartmovement::performdelayedstop();
    moveforward();
    vehstate = mforward;
    smartmovement::schedulestop( delay );
}



//perform delayed stop, if it exists, then start moving backwards,
//schedule a stop, and update the vehicle state
void smartmovement::smartmovebackwards( unsigned long int delay ) {
    smartmovement::performdelayedstop();
    movebackwards();
    vehstate = mback;
    smartmovement::schedulestop( delay );
}
