//handle movement in a more smart way
#ifndef SMARTMOVEMENT_H
#define SMARTMOVEMENT_H


#include "advmovement.h"



//define an enum for every state the vehicle can be in
enum statetype { rotcw, rotacw, mforward, mback, still };



//store the stop time here for pseudo async
static unsigned long int stoptime = 0;

//store the vehicle state here; set to still initially, because 
//the vehicle is programmed to stand still upon initialization
static statetype vehstate = still;



//all smart movements
class smartmovement : private advmovement {
    public:
        //smartly stop
        static void smartstop();

        //schedule a delayed stop
        static void schedulestop( unsigned long stopdelay );

        //perform a delayed stop
        static void performdelayedstop();

        //smartly rotate and move; no delay for rotation, since it wouldn't
        //work on that vehicle anyways
        static void smartrotateanticlockwise();
        static void smartrotateclockwise();
        static void smartmoveforward( unsigned long int delay );
        static void smartmovebackwards( unsigned long int delay );
};


#endif
